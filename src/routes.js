var React = require("react"),
    Router = require("director").Router,
    router = new Router();

var MenuComponent = require("./components/menu.jsx"),
    GameComponent = require("./components/game.jsx"),
    HelpComponent = require("./components/help.jsx");

function render(Component, props) {
  React.render(React.createElement(Component, props),
               document.getElementById("app"));
}

router.on("/", function() {
  render(MenuComponent, null);
});

router.on("/help", function() {
  render(HelpComponent, null);
});

router.on("/game", function() {
  render(GameComponent, null);
});

router.configure({html5history: false});

module.exports = router;
