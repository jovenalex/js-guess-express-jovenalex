var fullList = [
  "Gazpacho",
  "Estornudo",
  "Entrecejo",
  "Posavasos",
  "Joylent",
  "Enjuto Mojamuto",
  "Patinete",
  "Escarabajo"
];

var list = fullList.slice();

module.exports = {
  resetList: function() {
    list = fullList.slice();
  },
  getRandomWord: function() {
    var i = Math.floor(Math.random() * list.length),
        word = list[i];
    list.splice(i, 1);
    console.log("?", list);
    return word || "-- end --";
  }
};
