var React = require("react"),
    wordList = require("../word_list");

module.exports = React.createClass({
  getInitialState: function() {
    wordList.resetList();
    return this.newState();
  },
  componentDidMount: function() {
    this.updateSeconds();
  },
  updateSeconds: function() {
    this.setState({seconds: this.state.seconds - 1});
    if (this.state.seconds > 0) {
      this.timerId = setTimeout(this.updateSeconds, 1000);
    } else {
      alert("Se acabo el tiempo!");
    }
  },
  newState: function() {
    return {seconds: 60, word: wordList.getRandomWord() };
  },
  reset: function() {
    clearTimeout(this.timerId);
    this.setState(this.newState());
    this.timerId = setTimeout(this.updateSeconds, 1000);
  },
  render: function() {
    var seconds = this.state.seconds,
        word = this.state.word;
    return (
      <div>
          <h3>{word}</h3>
          <h4>{seconds}</h4>
          <a href="#/game" onClick={this.reset}>Skip!</a>
      </div>
    );
  }
});
