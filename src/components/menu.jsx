var React = require('react');

module.exports = React.createClass({
  render: function() {
    return (
      <div>
          <h1>Guess Express!</h1>
          <ul>
              <li><a href="#/help">How to Play</a></li>
              <li><a href="#/game">Start!</a></li>
          </ul>
      </div>
    );
  }
});
